-- *********************************************
-- * SQL PostgreSQL generation                 
-- *--------------------------------------------
-- * DB-MAIN version: 10.0.3              
-- * Generator date: Aug 17 2017              
-- * Generation date: Wed Apr 11 16:20:26 2018 
-- * LUN file: C:\Users\minad\eclipse-workspace\glide06\analyse\glide06.lun 
-- * Schema: E/A logique fin 2/1 
-- ********************************************* 
---******FIRST CREATE DATABASE******----
create database topglider;
/*** YOU NEED TO DO A MANUAL CHANGE OF POSITION IN THE DATABASE ***/
------******NEXT CREATE TABLE*******----
create table Pilote (
     IDPilote SERIAL UNIQUE,
     nom varchar(20) not null,
     email varchar(50) not null,
     prenom varchar(20) not null,
     adresse varchar(60) not null,
     gsm varchar(20) not null,
     solde float(1) not null,
     constraint ID_Pilote primary key (IDPilote));

create table TypePlaneur (
     IDPlaneur SERIAL UNIQUE,
     type varchar(20) not null,
     prixHoraire float(1) not null,
     description varchar(100) not null,
     constraint ID_TypePlaneur primary key (IDPlaneur));

create table Vol (
     IDVol SERIAL UNIQUE,
     dureeVol float(10) not null,
     date DATE not null,
     cout float(1) not null,
     IDPilote integer not null,
     IDPlaneur integer not null,
     constraint ID_Vol primary key (IDVol));


-- Constraints Section
-- ___________________ 

alter table Vol add constraint FKpilote_FK
     foreign key (IDPilote)
     references Pilote;

alter table Vol add constraint FKappartient_FK
     foreign key (IDPlaneur)
     references TypePlaneur;


-- Index Section
-- _____________ 

create index FKpilote_IND
     on Vol (IDPilote);

create index FKappartient_IND
     on Vol (IDPlaneur);
	 
-- Insertion des planeurs
-- _____________  
INSERT INTO typeplaneur(
            idplaneur, type, prixhoraire, description)
    VALUES (1,'bois & toile',17.0,'planeur en bois & en toile'),(2,'plastique',27.0,'planeur en plastique'),(3,'biplace',30.0,'planeur pour 2 personnes');
