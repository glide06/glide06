package be.iesca.usecaseimpl;

import java.util.List;

import be.iesca.dao.TypePlaneurDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.TypePlaneur;
import be.iesca.usecase.GestionTypePlaneurs;

public class GestionTypePlaneursImpl implements GestionTypePlaneurs {
	private TypePlaneurDao typePlaneurDao;

	public GestionTypePlaneursImpl() {
		this.typePlaneurDao = (TypePlaneurDao) DaoFactory.getInstance().getDaoImpl(TypePlaneurDao.class);
	}
	
	@Override
	public void rechercherTypePlaneur(Bundle bundle) {
		boolean operationReussie = false;
		int id = (int) bundle.get(Bundle.ID_TYPEPLANEUR);
		TypePlaneur typePlaneur = this.typePlaneurDao.rechercherTypePlaneur(id);
		String message = "";
		if (typePlaneur== null) {
			message = "Type de planeur inconnu";
		} else {
			operationReussie = true;
		}
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.TYPEPLANEUR, typePlaneur);
		bundle.put(Bundle.MESSAGE, message);
	}

	@Override
	public void listerTypePlaneur(Bundle bundle) {
		boolean listeOk = true;
		String message = "";
		List<TypePlaneur> listeTypePlaneur = null;
		listeTypePlaneur = this.typePlaneurDao.listerTypePlaneur();
		if (listeTypePlaneur==null) {
			listeOk = false;
		} else if (listeTypePlaneur.isEmpty())
			message = "Liste vide";
		else if (listeTypePlaneur.size() == 1)
			message = "Il y a 1 type de planeur";
		else
			message = "Il y a " + listeTypePlaneur.size() + " types de planeur";
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTEPLANEUR, listeTypePlaneur);
		
	}

}
