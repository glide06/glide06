package be.iesca.usecaseimpl;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import be.iesca.dao.VolDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.daoimpl.VolDaoImpl;
import be.iesca.daoimpl.VolDaoMockImpl;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Vol;
import be.iesca.usecase.GestionVols;

@SuppressWarnings("unused")
public class GestionVolsImpl  implements GestionVols{

	private VolDao volDao;

	public GestionVolsImpl() {
		this.volDao = (VolDao) DaoFactory.getInstance().getDaoImpl(VolDao.class);
		//this.volDao = (VolDao) new VolDaoMockImpl();
		//this.volDao = (VolDao) new VolDaoImpl();
	}	
	@Override
	public void ajouterVol(Bundle bundle) {
		boolean ajoutReussi = false;
		Date dateActuelle = new Date(Calendar.getInstance().getTimeInMillis());
		String message = "L'ajout n'a pas pu �tre r�alis�. Il manque : ";
		Vol vol = (Vol) bundle.get(Bundle.VOL);
		if (vol.getDureeVol() <= 0) message+= "la dur�e du vol, ";
		if (vol.getDureeVol() >=1440) message= "le vol ne peut pas durer un jour! ";
	    if (vol.getDate() == null) message+= "la date, ";
		if (vol.getIdPilote()<= 0 ) message+= "le pilote, ";
		if (vol.getIdPlaneur() <= 0) message+= "le type de planeur, ";
		if (vol.getCout() <= 0)message+= "le cout du vol ";
		if (vol.getDate().after(dateActuelle)) message = "La date ne peut pas �tre sup�rieure � celle d'aujourd'hui !";
		if(message == "L'ajout n'a pas pu �tre r�alis�. Il manque : ")
		{	
			ajoutReussi = this.volDao.ajouterVol(vol);
			if (ajoutReussi) {
				message = "Ajout r�alis�e avec succ�s";
			} else {
				message = "L'ajout n'a pas pu �tre r�alis�e";
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);
	}
	@Override
	public void listerVol(Bundle bundle,Date date) {
		boolean listeOk = true;
		String message = "";
		List<Vol> listeVols = null;
		listeVols = this.volDao.listerVols(date);
		if (listeVols==null) {
			listeOk = false;
		} else if (listeVols.isEmpty())
			message = "Liste vide";
		else if (listeVols.size() == 1)
			message = "Il y a 1 vol";
		else
			message = "Il y a " + listeVols.size() + " vols";
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTEVOL, listeVols);
		
	}

}
