package be.iesca.usecaseimpl;

import java.util.List;

import be.iesca.dao.PiloteDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.usecase.GestionPilotes;

public class GestionPilotesImpl implements GestionPilotes {

	private PiloteDao piloteDao;

	public GestionPilotesImpl() {
		// this.piloteDao =(PiloteDao) new PiloteDaoImpl();
		this.piloteDao = (PiloteDao) DaoFactory.getInstance().getDaoImpl(PiloteDao.class);
		// this.piloteDao =(PiloteDao) new PiloteDaoMockImpl();
	}

	@Override
	public void ajouterPilote(Bundle bundle) {
		boolean ajoutReussi = false;
		String message = "L'ajout n'a pas pu �tre r�alis�. Il manque : ";
		Pilote pilote = (Pilote) bundle.get(Bundle.PILOTE);
		if (pilote.getNom() == null || pilote.getNom().isEmpty())
			message += "le nom, ";
		if (pilote.getPrenom() == null || pilote.getPrenom().isEmpty())
			message += "le pr�nom, ";
		if (pilote.getAdresse() == null || pilote.getAdresse().isEmpty())
			message += "l'adresse, ";
		if (pilote.getEmail() == null || pilote.getEmail().isEmpty())
			message += "l'email, ";
		if (pilote.getGsm() == null || pilote.getGsm().isEmpty())
			message += "le numero de GSM ";
		if (message == "L'ajout n'a pas pu �tre r�alis�. Il manque : ") {
			Pilote piloteDB = this.piloteDao.getPilote(pilote.getEmail());
			if (piloteDB != null) {
				message = "L'ajout n'a pas pu �tre r�alis�. Ce pilote existe d�j�";
			} else {
				ajoutReussi = this.piloteDao.ajouterPilote(pilote);
				if (ajoutReussi) {
					message = "Ajout r�alis� avec succ�s";
				} else {
					message = "L'ajout n'a pas pu �tre r�alis�";
				}
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);

	}

	
	
	
	
	@Override
	public void rechercherPilote(Bundle bundle) {
		boolean operationReussie = false;
		String mail = (String) bundle.get(Bundle.MAIL);
		Pilote pilote = this.piloteDao.getPilote(mail);
		String message = "";
		if (pilote == null) {
			message = "Pilote inconnu";
		} else {
			operationReussie = true;
		}
		bundle.put(Bundle.MAIL, mail);
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.PILOTE, pilote);
		bundle.put(Bundle.MESSAGE, message);
	}

	public void listerPiloteTrier(Bundle bundle) {
		boolean listeOk = true;
		String message = "";
		List<Pilote> listePilotes = null;
		listePilotes = this.piloteDao.listerPilotesTri();
		if (listePilotes == null) {
			listeOk = false;
		} else if (listePilotes.isEmpty())
			message = "Liste vide";
		else if (listePilotes.size() == 1)
			message = "Il y a 1 pilote";
		else {
			message = "Il y a " + listePilotes.size() + " pilotes";
		}
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTEPILOTE, listePilotes);

	}

	@Override
	public void listerPiloteParSolde(Bundle bundle) {
		boolean listeOk = true;
		String message = "";
		List<Pilote> listePilotes = null;
		listePilotes = this.piloteDao.listerPilotesParSolde();
		if (listePilotes == null) {
			listeOk = false;
		} else if (listePilotes.isEmpty())
			message = "Liste vide";
		else if (listePilotes.size() == 1)
			message = "Il y a 1 pilote";
		else {
			message = "Il y a " + listePilotes.size() + " pilotes";
		}
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTEPILOTE, listePilotes);

	}

	@Override
	public void modifierPilote(Bundle bundle) {
		boolean modificationReussie = false;
		String message = "";
		Pilote pilote = (Pilote) bundle.get(Bundle.PILOTE);
		if (pilote.getNom() == null || pilote.getNom().isEmpty()) {
			message = "La modification n'a pas pu �tre r�alis�e. Il manque le nom du pilote";
		} else if (pilote.getPrenom() == null || pilote.getPrenom().isEmpty()) {
			message = "La modification n'a pas pu �tre r�alis�e. Il manque le pr�nom du pilote";
		} else if (pilote.getAdresse() == null || pilote.getAdresse().isEmpty()) {
			message = "La modification n'a pas pu �tre r�alis�e. Il manque l'adresse du pilote";
		} else if (pilote.getEmail() == null || pilote.getEmail().isEmpty()) {
			message = "La modification n'a pas pu �tre r�alis�e. Il manque l'email du pilote";
		} else if (pilote.getGsm() == null || pilote.getGsm().isEmpty()) {
			message = "La modification n'a pas pu �tre r�alis�e. Il manque le numero de GSM du pilote";
		} else {
			Pilote piloteDB = this.piloteDao.getPilote(pilote.getEmail());
			if (piloteDB == null) {
				message = "La modification n'a pas pu �tre r�alis�e. Ce pilote n'existe pas";
			} else {
				modificationReussie = this.piloteDao.modifierPilote(pilote);
				if (modificationReussie) {
					message = "Modification r�alis�e avec succ�s";
				} else {
					message = "La modification n'a pas pu �tre r�alis�e";
				}
			}
		}

		bundle.put(Bundle.OPERATION_REUSSIE, modificationReussie);
		bundle.put(Bundle.MESSAGE, message);

	}

	@Override
	public void rechercherPiloteByID(Bundle bundle) {
		boolean operationReussie = false;
		int id = (int) bundle.get(Bundle.ID_PILOTE);
		Pilote pilote = this.piloteDao.getPiloteByID(id);
		String message = "";
		if (pilote == null) {
			message = "Pilote inconnu";
		} else {
			operationReussie = true;
		}
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.PILOTE, pilote);
		bundle.put(Bundle.MESSAGE, message);
	}

}
