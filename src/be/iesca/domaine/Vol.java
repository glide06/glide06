package be.iesca.domaine;

import java.sql.Date;

public class Vol {
	@Override
	public String toString() {
		return "Vol [IdVol=" + IdVol + ", dureeVol=" + dureeVol + ", date=" + date + ", IdPilote=" + IdPilote
				+ ", IdPlaneur=" + IdPlaneur + ", cout=" + cout + "]";
	}
	private int IdVol;
	private float dureeVol;
	private Date date;
	private int IdPilote;
	private int IdPlaneur;
	private float cout;
	public Vol(int idVol, float dureeVol, Date date, int idPilote, int idPlaneur, float cout) {
		super();
		this.IdVol = idVol;
		this.dureeVol = dureeVol;
		this.date = date;
		this.IdPilote = idPilote;
		this.IdPlaneur = idPlaneur;
		this.cout = cout;
	}
	public Vol(Vol vol) {
		super();
		this.IdVol = vol.IdVol;
		this.dureeVol = vol.dureeVol;
		this.date = vol.date;
		this.IdPilote = vol.IdPilote;
		this.IdPlaneur = vol.IdPlaneur;
		this.cout = vol.cout;
	}
	public Vol( float dureeVol, Date date, int idPilote, int idPlaneur, float cout) {
		super();
		this.dureeVol = dureeVol;
		this.date = date;
		this.IdPilote = idPilote;
		this.IdPlaneur = idPlaneur;
		this.cout = cout;
	}
	public int getIdVol() {
		return IdVol;
	}
	public void setIdVol(int idVol) {
		IdVol = idVol;
	}
	public float getDureeVol() {
		return dureeVol;
	}
	public void setDureeVol(float dureeVol) {
		this.dureeVol = dureeVol;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getIdPilote() {
		return IdPilote;
	}
	public void setIdPilote(int idPilote) {
		IdPilote = idPilote;
	}
	public int getIdPlaneur() {
		return IdPlaneur;
	}
	public void setIdPlaneur(int idPlaneur) {
		IdPlaneur = idPlaneur;
	}
	public float getCout() {
		return cout;
	}
	public void setCout(float cout) {
		this.cout = cout;
	}	

}
