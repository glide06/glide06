package be.iesca.domaine;

import java.util.HashMap;
import java.util.Map;

public class Bundle {
	public static final String MESSAGE = "message";
	public static final String PILOTE = "pilote";
	public static final String TYPEPLANEUR = "typeplaneur";
	public static final String VOL = "vol";
	public static final String OPERATION_REUSSIE = "operationReussie";
	public static final String LISTEPILOTE = "listePilote";
	public static final String LISTEPLANEUR = "listePlaneur";
	public static final String LISTEVOL = "listeVol";
	public static final String MAIL = "mail";
	public static final String ID_TYPEPLANEUR = "idtypeplaneur";
	public static final String ID_PILOTE = "idpilote";

	private Map<String, Object> map = new HashMap<String, Object>();

	public Bundle() {
		this.map.put(Bundle.OPERATION_REUSSIE, false);
		this.map.put(Bundle.MESSAGE, "");
	}

	public void put(String clef, Object valeur) {
		this.map.put(clef, valeur);
	}

	public Object get(String clef) {
		return this.map.get(clef);
	}

	public void vider() {
		this.map.clear();
	}
}