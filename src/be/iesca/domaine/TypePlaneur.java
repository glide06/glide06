package be.iesca.domaine;

public class TypePlaneur {
	private int IDPlaneur;
	private String type;
	private float prixHoraire;
	private String description;
	
	public TypePlaneur(int iDPlaneur, String type, float prixHoraire, String description) {
		super();
		IDPlaneur = iDPlaneur;
		this.type = type;
		this.prixHoraire = prixHoraire;
		this.description = description;
	}
	
	public TypePlaneur(TypePlaneur typePlaneur) {
		IDPlaneur = typePlaneur.IDPlaneur;
		type = typePlaneur.type;
		prixHoraire = typePlaneur.prixHoraire;
		description = typePlaneur.description;
	}
	
	public TypePlaneur(String type) {
		this.type = type;
	}

	public void setIDPlaneur(int iDPlaneur) {
		IDPlaneur = iDPlaneur;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setPrixHoraire(float prixHoraire) {
		this.prixHoraire = prixHoraire;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIDPlaneur() {
		return IDPlaneur;
	}

	public String getType() {
		return type;
	}

	public float getPrixHoraire() {
		return prixHoraire;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + IDPlaneur;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + Float.floatToIntBits(prixHoraire);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypePlaneur other = (TypePlaneur) obj;
		if (IDPlaneur != other.IDPlaneur)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (Float.floatToIntBits(prixHoraire) != Float.floatToIntBits(other.prixHoraire))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (prixHoraire != 0)
			return type + " " + prixHoraire;
		else
			return type;
	}
	
	
}
