package be.iesca.domaine;

public class Pilote {
	private int IdPilote;
	private String nom;
	private String prenom;
	private String email;
	private String adresse;
	private String gsm;
	private float solde;

	public Pilote(int IdPilote, String nom, String prenom, String email, String adresse, String gsm, float solde) {
		super();
		this.IdPilote = IdPilote;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.adresse = adresse;
		this.gsm = gsm;
		this.solde = solde;
	}

	public Pilote(String nom, String prenom, String email, String adresse, String gsm, float solde) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.adresse = adresse;
		this.gsm = gsm;
		this.solde = solde;
	}

	public Pilote(String nom, String prenom, String email, String adresse, String gsm) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.adresse = adresse;
		this.gsm = gsm;
	}

	public Pilote(Pilote pilote) {
		super();
		this.IdPilote=pilote.IdPilote;
		this.nom = pilote.nom;
		this.prenom = pilote.prenom;
		this.email = pilote.email;
		this.adresse = pilote.adresse;
		this.gsm = pilote.gsm;
		this.solde = pilote.solde;
	}
	
	public Pilote(String email) {
		this.email=email;
	}

	public int getIdPilote() {
		return IdPilote;
	}

	public void setIdPilote(int idPilote) {
		IdPilote = idPilote;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getGsm() {
		return gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pilote other = (Pilote) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (gsm == null) {
			if (other.gsm != null)
				return false;
		} else if (!gsm.equals(other.gsm))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if (nom != null)
			return prenom + " " + nom;
		else
			return email;
	}

}
