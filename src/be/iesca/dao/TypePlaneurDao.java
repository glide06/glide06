package be.iesca.dao;

import java.util.List;

import be.iesca.domaine.TypePlaneur;

public interface TypePlaneurDao extends Dao {

	TypePlaneur rechercherTypePlaneur(int id);

	List<TypePlaneur> listerTypePlaneur();

}
