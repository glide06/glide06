package be.iesca.dao;

import java.util.List;

import be.iesca.domaine.Pilote;

public interface PiloteDao extends Dao {

	Pilote getPilote(String nom);
	
	Pilote getPiloteByID(int id);

	boolean ajouterPilote(Pilote pilote);

	boolean modifierPilote(Pilote pilote);

	List<Pilote> listerPilotesParSolde();

	List<Pilote> listerPilotesTri();

	

}
