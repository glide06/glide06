package be.iesca.dao;

import java.sql.Date;
import java.util.List;

import be.iesca.domaine.Vol;


public interface VolDao extends Dao {

	boolean ajouterVol(Vol vol);

	List<Vol> listerVols(Date date);

}
