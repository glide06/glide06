package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;

@SuppressWarnings("serial")
public class VueControllerModifierPilote extends JPanel implements ActionListener {
	private Model model;
	private Bundle bundle;
	private JButton jbModifier = new JButton("Modifier");
	private JButton jbReinitialiser = new JButton("R�initialiser");
	private JTextField jtfNom = new JTextField(20);
	private JTextField jtfPrenom = new JTextField(20);
	private JTextField jtfAdresse = new JTextField(100);
	private JTextField jtfMail = new JTextField(50);
	private JTextField jtfGsm = new JTextField(15);
	private JTextField jtfSolde = new JTextField(5);
	private GestionnaireUseCases gestionnaire;
	private Pilote pilote;
	public VueControllerModifierPilote(Model model,Pilote pilote) {
		if(pilote==null) throw new IllegalArgumentException("le pilote ne peut etre null");
		if (model != null) {
			this.model = model;
			this.bundle=this.model.getBundle();
			
		}
		this.pilote=pilote;
		this.gestionnaire = GestionnaireUseCases.getInstance();
		this.setLayout(new BorderLayout());
		this.add(creerPanelSaisies(), BorderLayout.CENTER);
		initialiser();
		this.jtfMail.setEditable(false);

	}
	public void setModel(Model model) {
		if (model != null) this.model = model;
		this.bundle=this.model.getBundle();
	}
	private JPanel creerPanelSaisies() {
		JPanel jpSaisies = new JPanel(new GridLayout(7, 2, 6, 2));
		jpSaisies.setPreferredSize(new Dimension(600, 300));
		jbModifier.addActionListener(this);
		jbReinitialiser.addActionListener(e -> reinitialiser());
		jpSaisies.add(new JLabel("Nom"));
		try {
			// Ajout d'un controle pour le nom : 20 caract�re et seulement des
			// lettres et espaces
			MaskFormatter nom = new MaskFormatter("U*******************");
			nom.setInvalidCharacters("0123456789&{}-_=+:/;.,?!\"<>|@[�]$*�%#���)");
			this.jtfNom = new JFormattedTextField(nom);
			jpSaisies.add(jtfNom);
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		jpSaisies.add(new JLabel("Pr�nom"));
		try {
			// Ajout d'un controle pour le prenom : 20 caract�re et seulement
			// des lettres et espaces
			MaskFormatter prenom = new MaskFormatter("U*******************");
			prenom.setInvalidCharacters("0123456789&{}-_=+:/;.,?8\"<>|@[]�$*�%#���)");
			this.jtfPrenom = new JFormattedTextField(prenom);
			jpSaisies.add(jtfPrenom);
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		jpSaisies.add(new JLabel("Adresse"));
		jpSaisies.add(this.jtfAdresse);
		jpSaisies.add(new JLabel("Mail"));
		jpSaisies.add(this.jtfMail);
		jpSaisies.add(new JLabel("GSM"));
		try {
			// Ajout d'un controle pour le numero de gsm : 10 caract�re et bon
			// format
			MaskFormatter tel = new MaskFormatter("####/##-##-##");
			this.jtfGsm = new JFormattedTextField(tel);
			jpSaisies.add(jtfGsm);
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		jpSaisies.add(new JLabel("Solde"));
		jpSaisies.add(this.jtfSolde);
		jpSaisies.add(this.jbModifier);
		jpSaisies.add(this.jbReinitialiser);

		return jpSaisies;
	}
	private void reinitialiser() {
		jtfNom.setText("");
		jtfPrenom.setText("");
		jtfAdresse.setText("");
		jtfGsm.setText("");
		jtfSolde.setText("");

	}
	private void initialiser() {
		jtfNom.setText(this.pilote.getNom());
		jtfPrenom.setText(this.pilote.getPrenom());
		jtfAdresse.setText(this.pilote.getAdresse());
		jtfGsm.setText(this.pilote.getGsm());
		jtfSolde.setText(String.valueOf(this.pilote.getSolde()));
		jtfMail.setText(this.pilote.getEmail());

	}
	private void modifier() {
		String nom = jtfNom.getText().trim();
		String prenom = jtfPrenom.getText().trim();
		String adresse = jtfAdresse.getText().trim();
		String gsm = jtfGsm.getText().trim();
		float solde = 0;
		if(jtfSolde.getText() != null && jtfSolde.getText().equals("")) 
			solde = 0; 
		else 
			try { 
				solde = Float.parseFloat(jtfSolde.getText()); 

			} 
		catch(Exception e) { 
			// pas a bon format 
			e.printStackTrace(); 
			solde= 0; 
		}
		Pilote piloteTemp = new Pilote(nom, prenom,this.pilote.getEmail(),adresse, gsm,solde);
		bundle.put(Bundle.PILOTE, piloteTemp);
		this.gestionnaire.modifierPilote(bundle);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==this.jbModifier)
		{
			modifier();
			if (model == null)
				return;
			Pilote pilote = (Pilote) this.model.getBundle().get(Bundle.PILOTE);
			if (pilote != null) {
				String message =  (String) this.model.getBundle().get(Bundle.MESSAGE);
				javax.swing.JOptionPane.showMessageDialog(null,message); 
				VueControllerAjouterVol.MajListePilote();
			}
			this.model.setBundle(bundle);
		}

	}
	@SuppressWarnings("unused")
	private class TextEcout implements FocusListener {
		public void focusLost(FocusEvent e) {
			if (jtfMail.getText().indexOf("@") == -1) {
				jtfMail.setForeground(Color.red);
				jbModifier.setEnabled(false);
				jtfMail.setToolTipText("Format email : xxxx@xxx");
			} else {
				jtfMail.setForeground(Color.black);
				jbModifier.setEnabled(true);
				jtfMail.setToolTipText(null);
			}

		}
		public void focusGained(FocusEvent e) {
		}
	}

}
