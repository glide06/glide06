package be.iesca.ihm;

/**
 *  Vue permettant l'affichage d'une liste des pilotes
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import be.iesca.domaine.Pilote;
import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;

@SuppressWarnings("serial")
public class VueListe extends JPanel implements ChangeListener {
	private GestionnaireUseCases gestionnaire;
	private static final String[] nomsColonnes = { "Nom", "Pr�nom", "Email", "Adresse", "Gsm", "Solde" };
	private Model model;
	private JTable jtable;
	private Bundle bundle;
	private JPanel panelListe;
	private JPanel panelBouton;
	private JButton boutonTriSolde = new JButton("Tri par solde n�gatif");
	private JButton boutonTri = new JButton("Tri par nom et pr�nom");
	private int indiceTri;
	private JLabel label = new JLabel("Aucun pilote");

	private VueListe() {
		this.panelListe = new JPanel();
		this.panelBouton = new JPanel();
		this.setLayout(new BorderLayout());
		this.setSize(800, 400);
		this.jtable = new JTable();
		this.jtable.setEnabled(false);
		JScrollPane jspTable = new JScrollPane(this.jtable);
		jspTable.setPreferredSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
		panelListe.add(jspTable);
		this.add(panelListe, BorderLayout.CENTER);
	}

	public VueListe(Model model, int indice) {
		this();
		if (model != null) {
			this.model = model;
			this.bundle = this.model.getBundle();
			this.model.addChangeListener(this);
			majListe(indice);
		}
		this.boutonTri.addActionListener(e -> {
			majListe(0);
		});
		this.boutonTriSolde.addActionListener(e -> {
			majListe(1);
		});
		ajoutPopup(this.jtable);
		this.panelBouton.add(boutonTri, BorderLayout.WEST);
		this.panelBouton.add(boutonTriSolde, BorderLayout.EAST);
		this.add(panelBouton, BorderLayout.SOUTH);
	}

	private void ajoutPopup(JTable jtable) {
		TableMenu popUpTable = new TableMenu(this.model, this.jtable);
		jtable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				int rowPoint = jtable.rowAtPoint(e.getPoint());
				if (e.isPopupTrigger()) {
					popUpTable.show(e.getComponent(), e.getX(), e.getY());
					popUpTable.setPilote(rowPoint);
				}
			}
		});
	}

	private void majListe(int control) {
		if (model == null)
			return;
		this.gestionnaire = GestionnaireUseCases.getInstance();
		this.indiceTri = control;
		panelListe.setVisible(true);
		// verification de quel tri on utilise
		switch (control) {
		case 0:
			this.gestionnaire.listerPiloteTrier(bundle);
			this.boutonTriSolde.setEnabled(true);
			this.boutonTri.setEnabled(false);
			break;
		case 1:
			this.gestionnaire.listerPiloteParSolde(bundle);
			this.boutonTriSolde.setEnabled(false);
			this.boutonTri.setEnabled(true);
			break;
		default:
			this.gestionnaire.listerPiloteTrier(bundle);
			this.boutonTriSolde.setEnabled(true);
			this.boutonTri.setEnabled(false);
			break;
		}
		
		if ((Boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
			@SuppressWarnings("unchecked")
			List<Pilote> liste = (List<Pilote>) bundle.get(Bundle.LISTEPILOTE);
			if (liste.isEmpty()) {
				this.label.setVisible(true);
				this.label.setForeground(new Color(255, 0, 0));
				this.boutonTriSolde.setEnabled(false);
				this.boutonTri.setEnabled(true);
				panelListe.setVisible(false);
				this.add(label, BorderLayout.NORTH);
			} else {
				this.label.setVisible(false);
				String[][] donnees = new String[liste.size()][nomsColonnes.length];
				for (int i = 0; i < liste.size(); i++) {
					Pilote b = liste.get(i);
					donnees[i][0] = b.getNom();
					donnees[i][1] = b.getPrenom();
					donnees[i][2] = b.getEmail();
					donnees[i][3] = b.getAdresse();
					donnees[i][4] = b.getGsm();
					donnees[i][5] = Float.toString(b.getSolde());
				}
				this.jtable.setModel(new DefaultTableModel(donnees, nomsColonnes));
			}
		}
		this.repaint();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		majListe(indiceTri);
	}
}
