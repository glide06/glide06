package be.iesca.ihm;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

import be.iesca.domaine.Pilote;

@SuppressWarnings("serial")
public class TableMenu extends JPopupMenu implements ActionListener {
	private JMenuItem itemModifier;
	private Model model;
	private Pilote pilote;
	private JTable jtable;

	public TableMenu(Model model,JTable jtable) {
		if(jtable==null) throw new IllegalArgumentException("la jtable ne peut etre null");
		itemModifier = new JMenuItem("Modifier");
		itemModifier.addActionListener(this);
		this.add(itemModifier);
		if(model!=null) 
			this.model=model;
		
		this.jtable=jtable;
	}
	public void setModel(Model model) {
		if (model != null) this.model = model;
	}

	public void setPilote(int rowTableau) {
		String nom=(String) this.jtable.getModel().getValueAt(rowTableau, 0);
		String prenom=(String) this.jtable.getModel().getValueAt(rowTableau, 1);
		String mail=(String) this.jtable.getModel().getValueAt(rowTableau, 2);
		String adresse=(String) this.jtable.getModel().getValueAt(rowTableau, 3);
		String gsm=(String) this.jtable.getModel().getValueAt(rowTableau, 4);
		float solde= Float.parseFloat((String) this.jtable.getModel().getValueAt(rowTableau, 5));
		this.pilote=new Pilote(nom, prenom, mail, adresse, gsm, solde);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		VueControllerModifierPilote modifierPilote;
		if(e.getSource()== itemModifier) {
			modifierPilote = new VueControllerModifierPilote(model,pilote);
			JOptionPane.showConfirmDialog(this, modifierPilote,
					"Modifier le pilote",JOptionPane.CLOSED_OPTION,JOptionPane.PLAIN_MESSAGE);
		}
		
	}
	
}
