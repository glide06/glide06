package be.iesca.ihm;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.MaskFormatter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.domaine.TypePlaneur;
import be.iesca.domaine.Vol;

@SuppressWarnings("serial")
public class VueControllerAjouterVol extends JPanel implements ActionListener {
	private final static String NON_SELECTIONNABLE_PILOTE = " - Selectionner un pilote - ";
	private final String NON_SELECTIONNABLE_PLANEUR = " - Selectionner un planeur - ";

	private Model model;
	private static Bundle bundle;
	private static GestionnaireUseCases gestionnaire;
	private static List<Pilote> listePilote;
	private List<TypePlaneur> listePlaneur;

	private JTextField jtfDate = new JTextField();
	private SpinnerNumberModel sNModel = new SpinnerNumberModel(1.0,1.0,999999999.0,1.0); //'placeholder' de 1.0, valeur min  1.0, valeur max 1440minutes (24h), par pas de 1.0
	private JSpinner jsTemps = new JSpinner(sNModel); //JSpinner avec le model sNModel
	private static JComboBox<Pilote> comboPilote = new JComboBox<Pilote>();
	private JComboBox<TypePlaneur> comboPlaneur = new JComboBox<TypePlaneur>();
	private JButton buttonAjouter = new JButton("Ajouter");
	private JButton buttonReinitialiser = new JButton("R�initialiser");

	public VueControllerAjouterVol(Model model) {
		if (model != null) {
			this.model = model;
			bundle = this.model.getBundle();
		}
		gestionnaire = GestionnaireUseCases.getInstance();
		this.add(creerPanelSaisie());
		MajListePilote();
		initListePlaneur();
	}

	private JPanel creerPanelSaisie() {
		JPanel jpGeneral = new JPanel();
		jpGeneral.setLayout(new GridLayout(5, 2, 6, 6));
		jpGeneral.setPreferredSize(new Dimension(600, 300));

		// ComboboxPilote
		jpGeneral.add(new JLabel("Pilote"));
		jpGeneral.add(comboPilote);
		comboPilote.addActionListener(this);

		// ComboboxPlaneur
		jpGeneral.add(new JLabel("Planeur"));
		jpGeneral.add(comboPlaneur);
		comboPlaneur.addActionListener(this);

		// Date
		jpGeneral.add(new JLabel("Date"));
		try {
			MaskFormatter date = new MaskFormatter("####-##-##");
			date.setValidCharacters("0123456789");
			this.jtfDate = new JFormattedTextField(date);
		} catch (ParseException e1) {e1.printStackTrace(); }
		jpGeneral.add(jtfDate);
		
		// Dur�e
		jpGeneral.add(new JLabel("Dur�e en minutes"));
		jpGeneral.add(jsTemps);

		// Bouton valider
		jpGeneral.add(buttonAjouter);
		buttonAjouter.addActionListener(this);

		// Bouton r�initialiser
		jpGeneral.add(buttonReinitialiser);
		buttonReinitialiser.addActionListener(this);
		return jpGeneral;
	}

	public void ajouter() {
		Pilote p = (Pilote) comboPilote.getSelectedItem();
		bundle.put(Bundle.MAIL, p.getEmail());
		gestionnaire.rechercherPilote(bundle);
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE) == true) {
			float coutFinal = 0, tempsFloat = 0, prixMinute = 0;
			Pilote pilote = (Pilote) bundle.get(Bundle.PILOTE);
			Date date = Date.valueOf(jtfDate.getText());
			Double tempsRecup = (double) jsTemps.getValue();
			tempsFloat = tempsRecup.floatValue(); //tempsFloat est �gal � la valeur entr�e dans le spinner
			
			TypePlaneur planeurSelect = (TypePlaneur) comboPlaneur.getSelectedItem(); //Recup�re le planeur s�lect
			prixMinute = planeurSelect.getPrixHoraire()/60; 
			coutFinal = prixMinute*tempsFloat; 
	
			Vol vol = new Vol(tempsFloat, date, pilote.getIdPilote(), planeurSelect.getIDPlaneur(), coutFinal); //Creation du nouveau vol avec les bonnes valeurs
			bundle.put(Bundle.VOL, vol);
			gestionnaire.ajouterVol(bundle);
			if (vol != null) {
				String message = (String) this.model.getBundle().get(Bundle.MESSAGE);
				reinitialiser();
				JOptionPane.showMessageDialog(null, message);
				//Mise � jour du solde du pilote en BD
				pilote.setSolde(pilote.getSolde()-coutFinal);
				bundle.put(Bundle.PILOTE, pilote);
				gestionnaire.modifierPilote(bundle);
			}
			this.model.setBundle(bundle);
		}
	}

	private void reinitialiser() {
		jtfDate.setText("");
		jsTemps.setValue(1.0);
		comboPilote.setSelectedIndex(0);
		comboPlaneur.setSelectedIndex(0);
		buttonAjouter.setEnabled(false);
	}

	public void setModel(Model model) {
		if (model != null) this.model = model;
		bundle = this.model.getBundle();
	}

	@SuppressWarnings("unchecked")
	public static void MajListePilote() {
		gestionnaire.listerPiloteTrier(bundle);
		listePilote = (List<Pilote>) bundle.get(Bundle.LISTEPILOTE);
		comboPilote.removeAllItems();
		comboPilote.addItem(new Pilote(NON_SELECTIONNABLE_PILOTE));
		for (Pilote p : listePilote) {
			comboPilote.addItem(p);
		}
		comboPilote.setSelectedItem(0);
	}

	@SuppressWarnings("unchecked")
	public void initListePlaneur() {
		gestionnaire.listerTypePlaneur(bundle);
		listePlaneur = (List<TypePlaneur>) bundle.get(Bundle.LISTEPLANEUR);
		comboPlaneur.addItem(new TypePlaneur(NON_SELECTIONNABLE_PLANEUR));
		for(TypePlaneur tp : listePlaneur) {
			comboPlaneur.addItem(tp);
		}
		comboPlaneur.setSelectedItem(0);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == comboPilote || e.getSource() == comboPlaneur) {
			if (comboPilote.getSelectedIndex() == 0 && comboPlaneur.getSelectedIndex() == 0) buttonAjouter.setEnabled(false);
			else if(comboPilote.getSelectedIndex() == 0 && comboPlaneur.getSelectedIndex() != 0) buttonAjouter.setEnabled(false);
			else if(comboPilote.getSelectedIndex() != 0 && comboPlaneur.getSelectedIndex() == 0) buttonAjouter.setEnabled(false);
			else buttonAjouter.setEnabled(true);
		}		
		if(e.getSource() == buttonAjouter) ajouter();
		if(e.getSource() == buttonReinitialiser) reinitialiser();	
	}
}
