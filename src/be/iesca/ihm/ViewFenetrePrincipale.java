package be.iesca.ihm;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class ViewFenetrePrincipale extends JPanel implements ChangeListener {
	private Model model;
	private BufferedImage image;

	public ViewFenetrePrincipale(Model model) {
		this();
		if (model != null) {
			this.model = model;
			this.model.addChangeListener(this);
		}
	}

	public ViewFenetrePrincipale() {
		try {
			image = ImageIO.read(new File("images/volplaneur.jpg"));
			repaint();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setModel(Model model) {
		if (model != null) {
			this.model = model;
			this.model.addChangeListener(this);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {
			int w = this.getWidth();
			int h = this.getHeight();
			g.drawImage(image, 0, 0, w, h, this);
		}

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		repaint();
	}
}
