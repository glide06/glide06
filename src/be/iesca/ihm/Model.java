package be.iesca.ihm;

import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import be.iesca.domaine.Bundle;

public class Model {
	private Bundle bundle;
	private ArrayList<ChangeListener> listeVues;
	
	public Model() {
		this.bundle=new Bundle();
		this.listeVues = new ArrayList<ChangeListener>(5);
	}
	public Model(String message) {
		this();
		this.bundle.put(Bundle.MESSAGE, message);
	}
	
	public synchronized void addChangeListener(ChangeListener chl) {
		if (!listeVues.contains(chl)) {
			listeVues.add(chl);
		}
	}
	public synchronized void removeChangeListener(ChangeListener chl) {
		if (listeVues.contains(chl)) {
			listeVues.remove(chl);
		}
	}
	protected synchronized void traiterEvent(ChangeEvent e) {
		for (ChangeListener listener : listeVues) {
			listener.stateChanged(e);
		}
	}
	
	public Bundle getBundle() {
		return this.bundle;
	}
	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
		traiterEvent(new ChangeEvent(this));
	}
	
}
