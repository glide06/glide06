package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


@SuppressWarnings("serial")
public class FenetrePrincipale extends JFrame implements ActionListener {
	private Model model;
	private JPanel affichage = new JPanel();
	private CardLayout cardLayout = new CardLayout();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu menuPilote = new JMenu("Gestion pilotes");
	private JMenu menuVol = new JMenu("Gestion vols");
	private JMenuItem menuItemAddPilote = new JMenuItem("Ajouter un pilote");
	private JMenuItem menuItemListPilote = new JMenuItem("Lister les pilotes");
	private JMenuItem menuItemAddVol = new JMenuItem("Ajouter un vol");
	private JMenuItem menuItemListVol = new JMenuItem("Lister les vols");
	private JToolBar toolBar = new JToolBar();
	private JButton buttonAddPilote = new JButton();
	private JButton buttonListPilote = new JButton();
	private JButton buttonListNegatifPilote = new JButton();
	private JButton buttonAddVol = new JButton();
	private JButton buttonListVol = new JButton();

	private void initMenuBar() {
		// Menu pilote
		// Menu g�n�ral gestion pilotes
		menuPilote.setToolTipText("Permet de g�rer les pilotes du syst�me");
		menuPilote.setIcon(new ImageIcon("images/User.png"));

		// Sous menu ajouter pilote
		menuItemAddPilote.setToolTipText("Permet d'ajouter un pilote");
		menuItemAddPilote.setIcon(new ImageIcon("images/addUser.png"));
		menuItemAddPilote.addActionListener(this);

		// Sous menu liste pilote
		menuItemListPilote.setToolTipText("Permet de lister les pilotes");
		menuItemListPilote.setIcon(new ImageIcon("images/listUser.png"));
		menuItemListPilote.addActionListener(this);

		// Ajout des item dans le menu
		menuPilote.add(menuItemAddPilote);
		menuPilote.add(menuItemListPilote);

		// Menu Vol
		// Menu g�n�ral gestion vol
		menuVol.setToolTipText("Permet de g�rer les vols du syst�me");
		menuVol.setIcon(new ImageIcon("images/Vol.png"));

		// Sous menu ajouter vol
		menuItemAddVol.setToolTipText("Permet d'ajouter un vol");
		menuItemAddVol.setIcon(new ImageIcon("images/addVol.png"));
		menuItemAddVol.addActionListener(this);

		// Sous menu lister vol
		menuItemListVol.setToolTipText("Permet de lister les vols");
		menuItemListVol.setIcon(new ImageIcon("images/listVol.png"));
		menuItemListVol.addActionListener(this);

		// Ajout des item dans le menu
		menuVol.add(menuItemAddVol);
		menuVol.add(menuItemListVol);

		// Ajout des menu � menuBar
		menuBar.add(menuPilote);
		menuBar.add(menuVol);
		this.setJMenuBar(menuBar);
	}

	private JToolBar initToolBar() {
		// Ajout pilote
		buttonAddPilote.setIcon(new ImageIcon("images/addUser.png"));
		buttonAddPilote.setToolTipText("Permet d'ajouter un pilote");
		buttonAddPilote.addActionListener(this);

		// liste pilote
		buttonListPilote.setIcon(new ImageIcon("images/listUser.png"));
		buttonListPilote.setToolTipText("Permet de lister les pilotes par nom et pr�nom");
		buttonListPilote.addActionListener(this);

		// liste pilote
		buttonListNegatifPilote.setIcon(new ImageIcon("images/listNegatifUser.png"));
		buttonListNegatifPilote.setToolTipText("Permet de lister les pilotes par solde n�gatif");
		buttonListNegatifPilote.addActionListener(this);

		// ajouter vol
		buttonAddVol.setIcon(new ImageIcon("images/addVol.png"));
		buttonAddVol.setToolTipText("Permet d'ajouter un vol");
		buttonAddVol.addActionListener(this);

		// lister vol
		buttonListVol.setIcon(new ImageIcon("images/listVol.png"));
		buttonListVol.setToolTipText("Permet de lister les vols");
		buttonListVol.addActionListener(this);

		toolBar.add(buttonAddPilote);
		toolBar.add(buttonListPilote);
		toolBar.add(buttonListNegatifPilote);
		toolBar.add(buttonAddVol);
		toolBar.add(buttonListVol);

		toolBar.setFloatable(false);
		return toolBar;
	}

	public FenetrePrincipale(String titre) {
		this.model = new Model();
		this.setTitle(titre);
		this.setSize(800, 500);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		   
		}
		initMenuBar();
		initCards();
		this.add(initToolBar(), BorderLayout.NORTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(affichage);
	}

	private void initCards() {
		affichage.setLayout(cardLayout);
		affichage.add(new ViewFenetrePrincipale(model), "FENETRE_PRINCIPALE");
		affichage.add(new VueControllerAjoutPilote(model), "AJOUT_PILOTE");
		affichage.add(new VueControllerAjouterVol(model), "AJOUT_VOL");
		affichage.add(new VueListeVol(model), "LIST_VOL");
		affichage.add(new VueListe(model, 0), "LIST_PILOTE_PRENOM");
		affichage.add(new VueListe(model, 1), "LIST_PILOTE_SOLDE_NEGATIF");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == menuItemAddPilote || e.getSource() == buttonAddPilote)
			cardLayout.show(affichage, "AJOUT_PILOTE");
		else if (e.getSource() == menuItemListPilote || e.getSource() == buttonListPilote)
			cardLayout.show(affichage, "LIST_PILOTE_PRENOM");
		else if (e.getSource() == buttonListNegatifPilote)
			cardLayout.show(affichage, "LIST_PILOTE_SOLDE_NEGATIF");
		else if (e.getSource() == menuItemAddVol || e.getSource() == buttonAddVol)
			cardLayout.show(affichage, "AJOUT_VOL");
		else if (e.getSource() == menuItemListVol || e.getSource() == buttonListVol)
			cardLayout.show(affichage, "LIST_VOL");
		else
			cardLayout.show(affichage, "FENETRE_PRINCIPALE");
	}

}
