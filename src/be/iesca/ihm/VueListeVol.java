package be.iesca.ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.domaine.Vol;
import be.iesca.domaine.TypePlaneur;

@SuppressWarnings("serial")
public class VueListeVol extends JPanel implements ActionListener {
	private static final String[] nomsColonnes = { "Nom", "Pr�nom", "Dur�e", "Planeur", "Cout" };
	public Model model;
	public Bundle bundle;
	private GestionnaireUseCases gestionnaire;
	
	private JTextField jtfDate = new JTextField();
	JButton buttonValider = new JButton("Rechercher");
	private JTable jtListe= new JTable();
	private JPanel panelRecherche = new JPanel();
	
	private JLabel label = new JLabel("Aucun vol � cette date");
	private JPanel panelTable = new JPanel();
	private JPanel panelLabel = new JPanel();

	public VueListeVol(Model model) {
		this.model = model;
		bundle = model.getBundle();
		this.gestionnaire = GestionnaireUseCases.getInstance();
		
		//Date
		MaskFormatter date = null;
		try {
			date = new MaskFormatter("####-##-##");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		date.setValidCharacters("0123456789");
		jtfDate = new JFormattedTextField(date);
		jtfDate.setPreferredSize(new Dimension(200, 30));
		
		//Panel recherche
		panelRecherche.add(jtfDate);
		panelRecherche.add(buttonValider);
		this.add(panelRecherche);
		
		//Panel Table
		jtListe.setEnabled(false);
		JScrollPane jspTable = new JScrollPane(jtListe);
		panelTable.add(jspTable);
		panelTable.setVisible(false);
		this.add(panelTable);
		
		//Label si aucun vol
		panelLabel.add(label);
		panelLabel.setVisible(false);
		this.add(panelLabel);
		
		//Bouton valider
		buttonValider.addActionListener(e -> { majListe(); });
	}
	
	@SuppressWarnings("unchecked")
	public void majListe() {
		Date date = Date.valueOf(jtfDate.getText());
		this.gestionnaire.listerVol(this.bundle, date);
		List<Vol> listeVols = (List<Vol>) this.bundle.get(Bundle.LISTEVOL);	
		String[][] donnees = new String[listeVols.size()][nomsColonnes.length];
		
		if (listeVols.isEmpty()) {
			this.panelTable.setVisible(false);
			this.panelLabel.setVisible(true);
			this.label.setForeground(new Color(255, 0, 0));
		} else {
			int i =0;
			this.panelLabel.setVisible(false);
			this.panelTable.setVisible(true);
			
			for(Vol v : listeVols) {
				bundle.put(Bundle.ID_PILOTE, v.getIdPilote());
				this.gestionnaire.rechercherPiloteByID(bundle);
				Pilote p = (Pilote) bundle.get(Bundle.PILOTE);
				
				//L'ID du planeur dans le bundle, rechercher dans le bundle, prendre les infos du bundle
				bundle.put(Bundle.ID_TYPEPLANEUR, v.getIdPlaneur());
				this.gestionnaire.rechercherTypePlaneur(bundle);
				TypePlaneur tp = (TypePlaneur) bundle.get(Bundle.TYPEPLANEUR);
				
				//Ajout dans le tableau
				donnees[i][0] = p.getNom();
				donnees[i][1] = p.getPrenom();
				donnees[i][2] = (int) v.getDureeVol() + "";
				donnees[i][3] = tp.getType();
				donnees[i][4] = Float.toString(v.getCout());
				i++;
			}
			this.jtListe.setModel(new DefaultTableModel(donnees, nomsColonnes));
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

	}

}
