package be.iesca.ihm;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import be.iesca.controleur.GestionnaireUseCases;

@SuppressWarnings("serial")
public class ControllerFenetrePrincipale extends JPanel implements ActionListener {

	private JButton jbAjouterPilote = new JButton("Ajouter pilote");
	private JButton jbListerPlaneur = new JButton("Lister pilote");
	private Model model;
	@SuppressWarnings("unused")
	private GestionnaireUseCases gestionnaire;

	public ControllerFenetrePrincipale() {
		this.add(creerPanelBoutons());
		ajouterListenersBoutons();

	}

	public ControllerFenetrePrincipale(Model model) {
		this();
		if (model != null)
			this.model = model;

	}

	public void setModel(Model model) {
		if (model != null)
			this.model = model;
	}

	private JPanel creerPanelBoutons() {
		JPanel jpBoutons = new JPanel();
		jpBoutons.setLayout(new FlowLayout(FlowLayout.CENTER));
		jpBoutons.setBorder(new TitledBorder(""));
		jpBoutons.add(jbAjouterPilote);
		jpBoutons.add(jbListerPlaneur);
		return jpBoutons;
	}

	private void ajouterListenersBoutons() {
		// listeners boutons
		jbAjouterPilote.addActionListener(this);
		jbListerPlaneur.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		VueControllerAjoutPilote ajoutPilote;
		VueListe liste;
		if (a.getSource() == jbAjouterPilote) {
			ajoutPilote = new VueControllerAjoutPilote(this.model);
			JOptionPane.showConfirmDialog(this, ajoutPilote, "Ajouter un nouveau pilote", JOptionPane.CLOSED_OPTION,
					JOptionPane.PLAIN_MESSAGE);
		}
		if (a.getSource() == jbListerPlaneur) {
			liste = new VueListe(this.model, 0);
			JOptionPane.showConfirmDialog(this, liste, "Lister les pilotes", JOptionPane.CLOSED_OPTION,
					JOptionPane.PLAIN_MESSAGE);

		}
	}

}
