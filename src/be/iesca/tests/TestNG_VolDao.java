package be.iesca.tests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import java.util.ArrayList;

import static org.testng.AssertJUnit.assertTrue;

import java.sql.Date;
import java.util.List;

import be.iesca.dao.PiloteDao;
import be.iesca.dao.VolDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.daoimpl.VolDaoMockImpl;
import be.iesca.domaine.Pilote;
import be.iesca.domaine.Vol;

@SuppressWarnings("unused")
public class TestNG_VolDao {
	private List<Vol> vols;
	private List<Pilote> pilotes;
	// private VolDao volDao=(VolDao) new VolDaoMockImpl();
	private VolDao volDao = (VolDao) DaoFactory.getInstance().getDaoImpl(VolDao.class);
	private PiloteDao piloteDao = (PiloteDao) DaoFactory.getInstance().getDaoImpl(PiloteDao.class);
	private Date date;

	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserListePilotes() {
		pilotes = new ArrayList<Pilote>(3);
		// ajout des Pilotes (liste d�j� tri�e)
		pilotes.add(new Pilote("Detre", "Marion", "marion.detre", "Rue des exemples n�10", "0471/296931", 12));
		pilotes.add(new Pilote("Minadeo", "Robin ", "robin.minadeo", "Rue des exemples n�10", "0471/296931", 100));
		pilotes.add(new Pilote("Robberecht", "Maxime", "Robberechtmaxime3", "Quartier joseph gailly n�7", "0471/296931",
				65));
	}

	@Test
	public void testAjouterPilote() {
		for (Pilote b : pilotes) {
			assertTrue(piloteDao.ajouterPilote(b));
		}
		pilotes = piloteDao.listerPilotesTri();
	}

	@Test(dependsOnMethods = { "testAjouterPilote" })
	public void testAjouterVols() {
		vols = new ArrayList<Vol>(3);
		date = Date.valueOf("2018-05-09");
		vols.add(new Vol(1, 60, date, pilotes.get(0).getIdPilote(), 1, 300));
		vols.add(new Vol(2, 120, date, pilotes.get(1).getIdPilote(), 2, 400));
		vols.add(new Vol(3, 30, date, pilotes.get(2).getIdPilote(), 3, 850));
		for (Vol b : vols) {
			AssertJUnit.assertTrue(volDao.ajouterVol(b));
		}
	}

	@Test(dependsOnMethods = { "testAjouterVols" })
	public void testLister() {
		List<Vol> volObtenues = volDao.listerVols(date);
		for (int i = 0; i < volObtenues.size(); i++) {
			AssertJUnit.assertEquals(volObtenues.get(i).getDureeVol(), vols.get(i).getDureeVol());
		}
	}
}
