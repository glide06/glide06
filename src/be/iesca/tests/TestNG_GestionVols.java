package be.iesca.tests;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.domaine.Vol;
import be.iesca.usecase.GestionPilotes;
import be.iesca.usecase.GestionVols;
import be.iesca.usecaseimpl.GestionPilotesImpl;
import be.iesca.usecaseimpl.GestionVolsImpl;

public class TestNG_GestionVols {
	private List<Vol> vols;
	private GestionVols gestionVols;
	private Bundle bundle;
	private Date date;
	private GestionPilotes gestionPilotes;
	private List<Pilote> pilotes;
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiser() throws ParseException {
		bundle = new Bundle();
		gestionVols=new GestionVolsImpl();
		gestionPilotes = new GestionPilotesImpl();
		vols = new ArrayList<Vol>(2);
		pilotes = new ArrayList<Pilote>(2);
		date = Date.valueOf("2018-05-09");
		pilotes.add(new Pilote("Detr�", "Marion", "mariodetre@outlook.com",
				"Rue des exemples n�10","0471296931",12));
		pilotes.add(new Pilote("Minad�o", "Robin ", "minadrobi@outlook.com",
				"Rue des exemples n�10","0471296931",100));
	}
	@Test
	public void testAjouterPilote() {
		for (Pilote b : pilotes) {
			bundle.put(Bundle.PILOTE, b);
			gestionPilotes.ajouterPilote(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		}
	}
	@Test(dependsOnMethods = { "testAjouterPilote" })
	public void testAjouterVol() {
		gestionPilotes.listerPiloteTrier(bundle);
		@SuppressWarnings("unchecked")
		List<Pilote> liste = (List<Pilote>)bundle.get(Bundle.LISTEPILOTE);
		vols.add(new Vol(1,60,date,liste.get(0).getIdPilote(),1,300));
		vols.add(new Vol(2,120,date,liste.get(1).getIdPilote(),2,400));
		for (Vol b : vols) {
			bundle.put(Bundle.VOL, b);
			gestionVols.ajouterVol(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		}
		
		// tentative d'ajout d'un vol dont il manque des donn�es
		bundle.put(Bundle.VOL, new Vol(60,null,liste.get(0).getIdPilote(), 1,300));
		gestionVols.ajouterVol(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		bundle.put(Bundle.VOL, new Vol(0,null,liste.get(0).getIdPilote(), 1,300));
		gestionVols.ajouterVol(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		bundle.put(Bundle.VOL, new Vol(0,null,0, 1,300));
		gestionVols.ajouterVol(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		bundle.put(Bundle.VOL, new Vol(0,null,0, 0,300));
		gestionVols.ajouterVol(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		bundle.put(Bundle.VOL, new Vol(0,null,0, 0,0));
		gestionVols.ajouterVol(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	@Test(dependsOnMethods = { "testAjouterVol" })
	public void testLister() throws ParseException {
		gestionVols.listerVol(bundle, date);
		assertTrue((Boolean)bundle.get(Bundle.OPERATION_REUSSIE));	
	}
}
