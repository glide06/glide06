package be.iesca.tests;

import static org.testng.AssertJUnit.assertTrue;

import java.sql.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.domaine.Vol;

public class TestNG_GestionnaireUseCases {

	private GestionnaireUseCases gestionnaire;
	private Bundle bundle;
	private Date date;
	private Pilote pilote;
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiser() {
		bundle = new Bundle();
		
		gestionnaire = GestionnaireUseCases.getInstance();
	}
	
	@Test
	public void testAjouterPilote() {
		Pilote pilote = new Pilote("Robberecht", "Maxime", "Robberechtmaxime3@outlook.com",
				"Quartier Joseph Gailly","01841851",100);
		this.bundle.put(Bundle.PILOTE, pilote);
		this.gestionnaire.ajouterPilote(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}


	@Test(dependsOnMethods = { "testAjouterPilote" })
	public void testModifierPilote() {
		// l'op�ration doit �tre accept�e
		pilote = new Pilote("Robberecht!", "Max!", "Robberechtmaxime3@outlook.com",
				"rue des exemples","0471296931",0);
		this.bundle.put(Bundle.PILOTE, pilote);
		this.gestionnaire.modifierPilote(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	@SuppressWarnings("unchecked")
	@Test(dependsOnMethods = { "testModifierPilote" })
	public void testListePilote() {
		this.gestionnaire.listerPiloteTrier(bundle);
		List<Pilote> liste = (List<Pilote>)bundle.get(Bundle.LISTEPILOTE);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		pilote=liste.get(0);
	}
	@Test(dependsOnMethods = { "testListePilote" })
	public void testAjouterVol(){
		date = Date.valueOf("2018-05-09");
		Vol vol = new Vol(60,date,pilote.getIdPilote(), 1,300);
		this.bundle.put(Bundle.VOL, vol);
		this.gestionnaire.ajouterVol(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}
	@Test(dependsOnMethods = { "testAjouterVol" })
	public void testListeVol() {
		this.gestionnaire.listerVol(bundle, date);
		@SuppressWarnings({ "unchecked", "unused" })
		List<Vol> liste = (List<Vol>)bundle.get(Bundle.LISTEVOL);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
	}

}
