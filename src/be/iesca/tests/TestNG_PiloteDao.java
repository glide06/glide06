package be.iesca.tests;
/*
 *  Tests unitaires de PiloteDao
 */
import static org.testng.AssertJUnit.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.PiloteDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.daoimpl.PiloteDaoImpl;
import be.iesca.daoimpl.PiloteDaoMockImpl;
import be.iesca.domaine.Pilote;

@SuppressWarnings("unused")
public class TestNG_PiloteDao {
	private List<Pilote> pilotes;
	//private PiloteDao piloteDao=(PiloteDao) new PiloteDaoImpl();
	private PiloteDao piloteDao=(PiloteDao)DaoFactory.getInstance().getDaoImpl(PiloteDao.class);
	//private PiloteDao piloteDao=(PiloteDao) new PiloteDaoMockImpl();
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserListePilotes() {
		pilotes = new ArrayList<Pilote>(3);
		// ajout des Pilotes (liste d�j� tri�e)
		pilotes.add(new Pilote("Detre", "Marion", "marion.detre",
				"Rue des exemples n�10","0471/296931",12));
		pilotes.add(new Pilote("Minadeo", "Robin ", "robin.minadeo",
				"Rue des exemples n�10","0471/296931",100));
		pilotes.add(new Pilote("Robberecht", "Maxime", "Robberechtmaxime3",
				"Quartier joseph gailly n�7","0471/296931",65));
	}


	@Test
	public void testAjouter() {
		for (Pilote b : pilotes) {
			assertTrue(piloteDao.ajouterPilote(b));
		}
	}

	@Test(dependsOnMethods = { "testAjouter" })
	public void testLister() {
		List<Pilote> pilotesObtenues = piloteDao.listerPilotesTri();
		for (int i = 0; i < pilotes.size(); i++) {
			assertEquals(pilotesObtenues.get(i).getNom(), pilotes.get(i).getNom());
		}
	}

	@Test(dependsOnMethods = { "testLister" })
	public void testRechercher() {
		for (Pilote b : pilotes) {
			
			Pilote piloteObtenue = piloteDao.getPilote(b.getEmail());
			assertEquals(piloteObtenue.getEmail(), b.getEmail());
		}
	}

	@Test(dependsOnMethods = { "testRechercher" })
	public void testModifier() {
		Pilote pilote = piloteDao.getPilote(pilotes.get(0).getEmail());
		pilote.setPrenom(pilote.getPrenom() + "!");
		pilote.setNom(pilote.getNom() + "!");
		pilote.setAdresse(pilote.getAdresse() + "!");
		pilote.setGsm(pilote.getGsm() + "!");
		assertTrue(piloteDao.modifierPilote(pilote));
		Pilote piloteObtenue = piloteDao.getPilote(pilote.getEmail());
		assertEquals(piloteObtenue.getEmail(), pilote.getEmail());
	}




}
