package be.iesca.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.Pilote;
import be.iesca.usecaseimpl.GestionPilotesImpl;

public class TestNG_ListerPilotes {
	
	private Bundle bundle;
	private GestionPilotesImpl gestionPilotes;
	private Pilote piloteU;
	private Pilote piloteZ;
	private Pilote piloteA;
	private ArrayList<Pilote> pilotes;
	@BeforeClass
	public void initialiser() {
		bundle = new Bundle();
		gestionPilotes=new GestionPilotesImpl();
		pilotes = new ArrayList<Pilote>(3);
		piloteU=new Pilote("Detr�", "Marion", "mariodetre@outlook.com",
				"Rue des exemples n�10","0471296931",-12);
		piloteZ=new Pilote("Minad�o", "Robin ", "minadrobi@outlook.com",
				"Rue des exemples n�10","0471296931",-100);
		piloteA=new Pilote("Robberecht", "Maxime", "Robberechtmaxime3@outlook.com",
				"Quartier joseph gailly n�7","0471296931",-65);
		pilotes.add(piloteU);
		pilotes.add(piloteZ);
		pilotes.add(piloteA);	
	}
	@Test
	public void testAjouter() {
		for (Pilote b : pilotes) {
			bundle.put(Bundle.PILOTE, b);
			gestionPilotes.ajouterPilote(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
			}
	}
	@Test(dependsOnMethods = { "testAjouter" })
	private void testListerTrierSolde() { 
		gestionPilotes.listerPiloteParSolde(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		@SuppressWarnings("unchecked")
		List<Pilote> pilotesObtenues = (List<Pilote>) bundle.get(Bundle.LISTEPILOTE);
		assertEquals( pilotesObtenues.get(0).getEmail(), piloteZ.getEmail());
		assertEquals( pilotesObtenues.get(1).getEmail(), piloteA.getEmail());
		assertEquals( pilotesObtenues.get(2).getEmail(), piloteU.getEmail());
	}
	@Test(dependsOnMethods = { "testListerTrierSolde" })
	private void testListerTrierNom() { 
		gestionPilotes.listerPiloteTrier(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		@SuppressWarnings("unchecked")
		List<Pilote> pilotesObtenues = (List<Pilote>) bundle.get(Bundle.LISTEPILOTE);
		assertEquals( pilotesObtenues.get(0).getEmail(), piloteU.getEmail());
		assertEquals( pilotesObtenues.get(1).getEmail(), piloteZ.getEmail());
		assertEquals( pilotesObtenues.get(2).getEmail(), piloteA.getEmail());
	}

}
