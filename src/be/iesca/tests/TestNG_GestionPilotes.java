package be.iesca.tests;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.domaine.Pilote;
import be.iesca.domaine.Bundle;
import be.iesca.usecase.GestionPilotes;
import be.iesca.usecaseimpl.GestionPilotesImpl;
/*
 * Tests fonctionnels du usecase GestionPilotes
 */
public class TestNG_GestionPilotes {

	private List<Pilote> pilotes;
	private GestionPilotes gestionPilotes;
	private Bundle bundle;

	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiser() {
		bundle = new Bundle();
		gestionPilotes=new GestionPilotesImpl();
		pilotes = new ArrayList<Pilote>(5);
		// ajout des pilotes (liste d�j� tri�e)
		pilotes.add(new Pilote("Detr�", "Marion", "mariodetre@outlook.com",
				"Rue des exemples n�10","0471296931",12));
		pilotes.add(new Pilote("Minadeo", "Robin ", "minadrobi@outlook.com",
				"Rue des exemples n�10","0471296931",100));
		pilotes.add(new Pilote("Robberecht", "Maxime", "Robberechtmaxime3@outlook.com",
				"Quartier joseph gailly n�7","0471296931",65));	
	}

	@Test
	public void testAjouter() {
		for (Pilote b : pilotes) {
			bundle.put(Bundle.PILOTE, b);
			gestionPilotes.ajouterPilote(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		}
		// tentative d'ajout d'un Pilote d�j� pr�sente
		bundle.put(Bundle.PILOTE, pilotes.get(0));
		gestionPilotes.ajouterPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		// tentative d'ajout d'un pilote dont il manque des donn�es
		bundle.put(Bundle.PILOTE, new Pilote(null,null,null,null,null,0));
		gestionPilotes.ajouterPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
		bundle.put(Bundle.PILOTE, new Pilote("nom",null,null,null,null,0));
		gestionPilotes.ajouterPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		bundle.put(Bundle.PILOTE, new Pilote("nom","prenom",null,null,null,0));
		gestionPilotes.ajouterPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		bundle.put(Bundle.PILOTE, new Pilote("nom","prenom","email",null,null,0));
		gestionPilotes.ajouterPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	@Test(dependsOnMethods = { "testAjouter" })
	public void testRechercher() {
		for (Pilote b : pilotes) {
			bundle.put(Bundle.MAIL, b.getEmail());
			gestionPilotes.rechercherPilote(bundle);
			Pilote piloteObtenue = (Pilote) bundle.get(Bundle.PILOTE);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
			assertEquals(piloteObtenue.getNom(), b.getNom());
		}
	}

	@Test(dependsOnMethods = { "testRechercher" })
	public void testModifier() {
		// modification Pilote existant
		bundle.put(Bundle.MAIL, pilotes.get(0).getEmail());
		gestionPilotes.rechercherPilote(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		Pilote pilote = (Pilote) bundle.get(Bundle.PILOTE);
		pilote.setPrenom(pilote.getPrenom() + "!");
		pilote.setNom(pilote.getNom() + "!");
		pilote.setAdresse(pilote.getAdresse() + "!");
		pilote.setGsm(pilote.getGsm() + "!");
		pilote.setSolde(pilote.getSolde());
		bundle.vider();
		bundle.put(Bundle.PILOTE, pilote);

		gestionPilotes.modifierPilote(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		Pilote piloteObtenue = (Pilote) bundle.get(Bundle.PILOTE);
		assertEquals(piloteObtenue, pilote);
		
		//tentative de modification d'un pilote existant
		bundle.put(Bundle.PILOTE, new Pilote("?", "?", "?", "?","?",0));
		gestionPilotes.modifierPilote(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		
	}
}