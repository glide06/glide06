package be.iesca.controleur;

import java.sql.Date;

import be.iesca.domaine.Bundle;
import be.iesca.usecase.GestionPilotes;
import be.iesca.usecase.GestionTypePlaneurs;
import be.iesca.usecase.GestionVols;
import be.iesca.usecaseimpl.GestionPilotesImpl;
import be.iesca.usecaseimpl.GestionTypePlaneursImpl;
import be.iesca.usecaseimpl.GestionVolsImpl;

/**
 * Contrôleur de l'application (couche logique) C'est un singleton.
 * 
 *
 */
public class GestionnaireUseCases implements GestionPilotes, GestionVols, GestionTypePlaneurs {

	private static final GestionnaireUseCases INSTANCE = new GestionnaireUseCases();
	private GestionPilotes gestionPilotes;
	private GestionVols gestionVols;
	private GestionTypePlaneurs gestionTypePlaneurs;

	public static GestionnaireUseCases getInstance() {
		return INSTANCE;
	}

	private GestionnaireUseCases() {
		this.gestionPilotes = new GestionPilotesImpl();
		this.gestionVols = new GestionVolsImpl();
		this.gestionTypePlaneurs = new GestionTypePlaneursImpl();
	}

	@Override
	public void ajouterPilote(Bundle bundle) {
		this.gestionPilotes.ajouterPilote(bundle);
	}

	@Override
	public void rechercherPilote(Bundle bundle) {
		this.gestionPilotes.rechercherPilote(bundle);
	}

	@Override
	public void listerPiloteParSolde(Bundle bundle) {
		this.gestionPilotes.listerPiloteParSolde(bundle);
	}

	@Override
	public void modifierPilote(Bundle bundle) {
		this.gestionPilotes.modifierPilote(bundle);
	}

	@Override
	public void rechercherTypePlaneur(Bundle bundle) {
		this.gestionTypePlaneurs.rechercherTypePlaneur(bundle);
	}

	@Override
	public void ajouterVol(Bundle bundle) {
		this.gestionVols.ajouterVol(bundle);
	}

	@Override
	public void listerPiloteTrier(Bundle bundle) {
		this.gestionPilotes.listerPiloteTrier(bundle);
	}

	@Override
	public void listerVol(Bundle bundle, Date date) {
		this.gestionVols.listerVol(bundle, date);
	}

	@Override
	public void listerTypePlaneur(Bundle bundle) {
		this.gestionTypePlaneurs.listerTypePlaneur(bundle);
	}

	@Override
	public void rechercherPiloteByID(Bundle bundle) {
		this.gestionPilotes.rechercherPiloteByID(bundle);
		
	}
}