package be.iesca.usecase;

import be.iesca.domaine.Bundle;

public interface GestionPilotes {
	void ajouterPilote(Bundle bundle);
	void rechercherPilote(Bundle bundle);
	void rechercherPiloteByID(Bundle bundle);
	void modifierPilote(Bundle bundle);
	void listerPiloteParSolde(Bundle bundle);
	void listerPiloteTrier(Bundle bundle);
}
