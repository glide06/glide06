package be.iesca.usecase;

import be.iesca.domaine.Bundle;

public interface GestionTypePlaneurs {
	void rechercherTypePlaneur(Bundle bundle);
	void listerTypePlaneur(Bundle bundle);
}
