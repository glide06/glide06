package be.iesca.usecase;

import java.sql.Date;

import be.iesca.domaine.Bundle;

public interface GestionVols {
	void ajouterVol(Bundle bundle);
	void listerVol(Bundle bundle,Date date);
	
}
