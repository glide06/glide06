package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import be.iesca.dao.TypePlaneurDao;
import be.iesca.domaine.TypePlaneur;

public class TypePlaneurDaoImpl implements TypePlaneurDao {
	private static final String GET = "SELECT * FROM typeplaneur p WHERE p.IDPlaneur = ?";
	private static final String LISTE = "SELECT * FROM typeplaneur";

	public TypePlaneurDaoImpl() {
	}

	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}
	
	@Override
	public TypePlaneur rechercherTypePlaneur(int id) {
		TypePlaneur typePlaneur = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GET);
			ps.setInt(1,id);
			rs = ps.executeQuery();
			if(rs.next()) {
				typePlaneur = new TypePlaneur(id,rs.getString("type"),rs.getFloat("prixHoraire"),rs.getString("description"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return typePlaneur;
	}

	@Override
	public List<TypePlaneur> listerTypePlaneur() {
		List<TypePlaneur> liste = new ArrayList<TypePlaneur>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTE);
			rs = ps.executeQuery();
			while (rs.next()) {
				TypePlaneur typePlaneur = new TypePlaneur(rs.getInt(1), rs.getString(2), rs.getFloat(3), rs.getString(4));
				liste.add(typePlaneur);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return liste;
	}

}
