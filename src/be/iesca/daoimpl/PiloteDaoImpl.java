package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import be.iesca.dao.PiloteDao;
import be.iesca.domaine.Pilote;

public class PiloteDaoImpl implements PiloteDao {
	private static final String GET = "SELECT * FROM pilote p WHERE p.email = ?";
	private static final String GETBYID = "SELECT * FROM pilote p WHERE p.idpilote = ?";
	private static final String AJOUT = "INSERT INTO pilote (nom,  prenom, email, adresse, gsm, solde) VALUES (?,?,?,?,?,?)";
	private static final String MAJ = "UPDATE pilote SET nom= ?, prenom= ?, adresse= ?, gsm= ?, solde= ? where email= ?";
	private static final String LISTERTRIEE = "SELECT * FROM pilote b ORDER BY b.nom, b.prenom";
	private static final String LISTERPILOTE = "SELECT * FROM pilote b where b.solde<0 ORDER BY b.solde  ";

	// obligatoire pour pouvoir construire une instance avec newInstance()
	public PiloteDaoImpl() {
	}

	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}

	@Override
	public Pilote getPilote(String email) {
		Pilote pilote = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GET);
			ps.setString(1, email.trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				pilote = new Pilote(rs.getInt("Idpilote"), rs.getString("nom"), rs.getString("prenom"), email,
						rs.getString("adresse"), rs.getString("gsm"), rs.getFloat("solde"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return pilote;
	}

	@Override
	public boolean ajouterPilote(Pilote pilote) {
		boolean ajoutReussi = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUT);
			ps.setString(1, pilote.getNom().trim());
			ps.setString(2, pilote.getPrenom().trim());
			ps.setString(3, pilote.getEmail().trim());
			ps.setString(4, pilote.getAdresse().trim());
			ps.setString(5, pilote.getGsm().trim());
			ps.setFloat(6, pilote.getSolde());
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajoutReussi = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajoutReussi;
	}

	@Override
	public List<Pilote> listerPilotesTri() {
		List<Pilote> liste = new ArrayList<Pilote>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERTRIEE);
			rs = ps.executeQuery();
			String nom = "";
			while (rs.next()) {
				nom = rs.getString("nom");
				Pilote pilote = new Pilote(rs.getInt(1), nom, rs.getString(4), rs.getString(3), rs.getString(5),
						rs.getString(6), rs.getFloat(7));
				liste.add(pilote);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return liste;
	}

	@Override
	public List<Pilote> listerPilotesParSolde() {
		List<Pilote> liste = new ArrayList<Pilote>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERPILOTE);
			rs = ps.executeQuery();
			String nom = "";
			while (rs.next()) {
				nom = rs.getString("nom");
				Pilote pilote = new Pilote(rs.getInt(1), nom, rs.getString(4), rs.getString(3), rs.getString(5),
						rs.getString(6), rs.getFloat(7));
				liste.add(pilote);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return liste;
	}

	@Override
	public boolean modifierPilote(Pilote pilote) {
		boolean modificationReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(MAJ);
			String mail = pilote.getEmail().trim();
			ps.setString(6, mail);
			ps.setString(2, pilote.getPrenom().trim());
			ps.setString(1, pilote.getNom().trim());
			ps.setString(3, pilote.getAdresse().trim());
			ps.setString(4, pilote.getGsm().trim());
			ps.setFloat(5, pilote.getSolde());
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				modificationReussie = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return modificationReussie;
	}

	@Override
	public Pilote getPiloteByID(int id) {
		Pilote pilote = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GETBYID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				pilote = new Pilote(id, rs.getString("nom"), rs.getString("prenom"), rs.getString("email"),
						rs.getString("adresse"), rs.getString("gsm"), rs.getFloat("solde"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return pilote;
	}

}
