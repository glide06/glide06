package be.iesca.daoimpl;


import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import be.iesca.dao.VolDao;
import be.iesca.domaine.Vol;

public class VolDaoMockImpl implements VolDao {
	private Map<Date,List<Vol>> mapVol;
	

	public VolDaoMockImpl() {
		super();
		this.mapVol = new TreeMap<Date,List<Vol>>();
	}


	@Override
	public boolean ajouterVol(Vol vol) {
		try {
			if (this.mapVol.containsKey(vol.getDate())){
			List<Vol> listeVol = this.mapVol.get(vol.getDate());
			if((boolean)listeVol.contains(vol)==false) {
				listeVol.add(vol);
			}
			}else {
				List<Vol> listeVol = new ArrayList<Vol>();
				listeVol.add(vol);
				this.mapVol.put(vol.getDate(),listeVol);
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
		
	}

	@Override
	public List<Vol> listerVols(Date date) {
		return this.mapVol.get(date);
		}

}
