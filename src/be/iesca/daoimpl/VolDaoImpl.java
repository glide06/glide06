package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import be.iesca.dao.VolDao;
import be.iesca.domaine.Vol;

public class VolDaoImpl implements VolDao {
	
	private static final String AJOUT = "INSERT INTO vol (dureevol,  date, cout, idpilote, idplaneur) VALUES (?,?,?,?,?)";
	private static final String LISTER = "SELECT * FROM vol b where b.date = ?";
	public VolDaoImpl() {
	}
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}
	@Override
	public boolean ajouterVol(Vol vol) {
		boolean ajoutReussi = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUT);
			ps.setFloat(1, vol.getDureeVol());
			ps.setDate(2,vol.getDate());
			ps.setFloat(3, vol.getCout());
			ps.setInt(4, vol.getIdPilote());
			ps.setInt(5, vol.getIdPlaneur());
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajoutReussi = true;
			}
		} catch (Exception ex) {
		ex.printStackTrace();
	} finally {
		cloturer(null, ps, con);
	}
		return ajoutReussi;
	}

	@Override
	public List<Vol> listerVols(Date date) {
		List<Vol> listeVol= new ArrayList<Vol>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTER);
			ps.setDate(1,date);
			rs = ps.executeQuery();
			while (rs.next()) {
				Vol vol = new Vol(rs.getInt(1),rs.getFloat(2),rs.getDate(3),rs.getInt(5), rs.getInt(6),rs.getFloat(4));
				listeVol.add(vol);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return listeVol;
	}

}
