package be.iesca.daoimpl;
	import java.util.ArrayList;
	import java.util.Collection;
	import java.util.Collections;
	import java.util.Comparator;
	import java.util.List;
	import java.util.Map;
	import java.util.TreeMap;

	import be.iesca.dao.PiloteDao;
	import be.iesca.domaine.Pilote;



public class PiloteDaoMockImpl implements PiloteDao {
			private Map<String, Pilote> mapPilotes;

		public PiloteDaoMockImpl() {
			Comparator<String> comp = new ComparateurPilotes();
			this.mapPilotes = new TreeMap<String, Pilote>(comp);
		}

		@Override
		public boolean ajouterPilote(Pilote pilote) {
			try {
				if (this.mapPilotes.containsKey(pilote.getEmail()))
					return false;
				this.mapPilotes.put(pilote.getEmail(), pilote);
			} catch (Exception ex) {
				return false;
			}
			return true;
		}
	


		@Override
		public boolean modifierPilote(Pilote pilote) {
			try {
				if (this.mapPilotes.remove(pilote.getEmail()) == null)
					return false;
				this.mapPilotes.put(pilote.getEmail(), pilote);
				return true;
			} catch (Exception ex) {
				return false;
			}
		}

		private class ComparateurPilotes implements Comparator<String> {
			@Override
			public int compare(String mail1, String mail2) {
				return mail1.compareTo(mail2);
			}
		}
	



	@Override
	public Pilote getPilote(String mail) {
		try {
			Pilote pilote = this.mapPilotes.get(mail);
			if ( pilote==null) return null;
			return new Pilote(pilote);
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public List<Pilote> listerPilotesParSolde() {	
		Collection<Pilote> col = Collections.unmodifiableCollection(this.mapPilotes.values());
	return new ArrayList<Pilote>(col);
	}

	@Override
	public List<Pilote> listerPilotesTri() {
		Collection<Pilote> col = Collections.unmodifiableCollection(this.mapPilotes.values());
	return new ArrayList<Pilote>(col);
	}

	@Override
	public Pilote getPiloteByID(int id) {
		try {
			Pilote pilote = this.mapPilotes.get(id);
			if ( pilote==null) return null;
			return new Pilote(pilote);
		} catch (Exception ex) {
			return null;
		}
	}


}
